package com.dsncode.auth.stepdef;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import com.dsncode.auth.repository.DBDriver;
import com.dsncode.auth.repository.DBRepository;
import com.dsncode.auth.service.AuthService;
import com.dsncode.auth.vo.User;

import org.mockito.Mockito;

public class AuthStepDef {

    Function<Map.Entry<String, String>, Optional<User>> entryToUser = (entry) -> {
        User user = null;
        try {
            user = User.createInstance(UUID.randomUUID(), entry.getKey(), entry.getValue());
        } catch (Exception e) {
            user = null;
        }
        return Optional.ofNullable(user);
    };

    String username = "";
    String password = "";
    String sessionToken = "";
    Exception exception;
    DBDriver driver;
    DBRepository db;
    AuthService service;

    @Given("^a initialized system$")
    public void a_initialized_system() throws Exception {
        driver = Mockito.mock(DBDriver.class);
        db = new DBRepository(driver);
        service = new AuthService(db);
    }

    @Given("^an existent user database$")
    public void an_existent_user_database(DataTable data) throws Exception {
        Map<String, String> map = data.asMap(String.class, String.class);
        map.entrySet().stream().map(entryToUser).filter(Optional::isPresent).map(Optional::get).forEach(user -> {
            try {
                db.registerUser(user);
            } catch (Exception e) {

            }
        });
    }

    @Given("^a user with username \"([^\"]*)\"$")
    public void a_user_with_username(String inputUsername) throws Exception {
        this.username = inputUsername;
    }

    @When("^the user input password \"([^\"]*)\"$")
    public void a_user_input_password(String inputPassword) throws Exception {
        this.password = inputPassword;
    }

    @When("^the user try to login$")
    public void the_user_try_to_login() throws Exception {
        try {
            this.sessionToken = service.login(username, password);
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @When("^the user try to register$")
    public void the_user_try_to_register() throws Exception {
        try {
            this.service.registerUser(username, password);
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @When("^the database is offline$")
    public void the_database_is_disconnected() throws Exception {
        when(driver.insert(any())).thenThrow(new Exception("Database disconnected"));
        when(driver.query(any())).thenThrow(new Exception("Database disconnected"));
    }

    @Then("^the system will throw an exception$")
    public void the_system_will_throw_an_exception() throws Exception {
        if (this.exception == null) {
            fail("system should have thrown an exception. but it did not");
        }
    }

    @Then("^the system will not throw an exception$")
    public void the_system_will_not_throw_an_exception() throws Exception {
        if (this.exception != null) {
            fail("system should not have thrown an exception");
        }
    }

    @Then("^session token should be \"([^\"]*)\"$")
    public void session_token_should_be(String arg1) throws Exception {
        boolean shouldBeEmpty = arg1.equals("empty");
        if (shouldBeEmpty) {
            if (this.sessionToken.isEmpty() == false) {
                fail("session should be empty");
            }
        } else {
            if (this.sessionToken.isEmpty()) {
                fail("session should not be empty");
            }
        }
    }
}