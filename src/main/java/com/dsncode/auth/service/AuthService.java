package com.dsncode.auth.service;

import java.util.UUID;

import com.dsncode.auth.repository.DBRepository;
import com.dsncode.auth.vo.User;

/**
 * AuthService
 */
public class AuthService {

    private DBRepository db;

    public AuthService(DBRepository db) {
        this.db = db;
    }

    private boolean isSafePassword(String password) {
        // secure definition is not null and more than 8 chars
        if (password != null && password.isEmpty() == false && password.length() >= 8) {
            return true;
        }
        return false;
    }

    public User registerUser(String username, String password) throws Exception {

        if (isSafePassword(password) == false) {
            throw new Exception("unsafe password");
        }
        // value object. it will thrown an exception if password is empty.
        User user = User.createInstance(UUID.randomUUID(), username, password);
        this.db.registerUser(user);
        return user;
    }

    public String login(String username, String password) throws Exception {

        if (db.isUserOnDB(username, password)) {
            return UUID.randomUUID().toString();
        }

        throw new Exception("login/password does not exists");
    }
}
