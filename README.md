# Auth Module Example using BDD with Cucumber

this module, uses BDD (Behavior driven development) approach using cucumber & Java. In order to understand a bit more how cucumber framework and BDD work together.

please check the [introduction here](http://bit.ly/2XSV02i)

## Requirements to run this project
- Java 8
- Maven

## To run all tests

just type
```bash
mvn test
```

and you will be able to see all scenarios and tests for this mock module. for example:
```bash
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running com.dsncode.auth.RunCucumberTest
Feature: does a user can register in our system?
    a user want to register into the system

  Scenario: A user log tries to register in the system, using an unsafe password # com/dsncode/auth/login.feature:4
    Given a user enter the system                                                # Stepdefs.a_user_enter_the_system()
    And a user with username "root"                                              # Stepdefs.a_user_with_username(String)
    When a user tries to register password "123"                                 # Stepdefs.a_user_tries_to_register_password(String)
    Then the system will "not" allow him to register his account                 # Stepdefs.the_system_will_allow_him_to_register_his_account(String)

  Scenario: A user tries to register using a safe passsword     # com/dsncode/auth/login.feature:10
    Given a user enter the system                               # Stepdefs.a_user_enter_the_system()
    And a user with username "root"                             # Stepdefs.a_user_with_username(String)
    When a user tries to register password "12345678"           # Stepdefs.a_user_tries_to_register_password(String)
    Then the system will "do" allow him to register his account # Stepdefs.the_system_will_allow_him_to_register_his_account(String)

2 Scenarios (2 passed)
8 Steps (8 passed)
0m0.067s

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.195 sec

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0
```